package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        compareWords(hiddenWord());

    }

    public static String hiddenWord(){

        String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli", "carrot", "cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive", "pea", "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};

        Random random = new Random ();
        int i = random.nextInt(words.length);
        return words[i];
    }

    public static String chosenWord() {

        Scanner scanner = new Scanner(System.in);
        System.out.println ("Choose the one: apple, orange, lemon, banana, apricot, avocado, broccoli, carrot, cherry, garlic, grape, melon, leak, kiwi, mango, mushroom, nut, olive, pea, peanut, pear, pepper, pineapple, pumpkin, potato ...");
        String word = scanner.nextLine();
        return word;
    }

    public static void compareWords(String word1) {

        String word2 = chosenWord();

        if (word1.contentEquals(word2)) {

            System.out.println("Congratulations! You won! The hidden word was" + " " + word1);
            return;
        }

        for (int i = 0; i<15; i++) {

            if (i<word1.length() && i<word2.length() && word1.charAt(i)==word2.charAt(i)) {
                System.out.print(word1.charAt(i));
            }

            else {
                System.out.print("#");
            }
        }

        System.out.println("Don't worry! Try again ... :) ");
        compareWords(word1);

        }
    }

